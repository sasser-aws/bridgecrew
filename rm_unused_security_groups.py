#!/usr/bin/env python

import boto3
import argparse
import datetime
import time
import sys
from botocore.exceptions import ClientError


def timestamp():
    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')


def get_all_security_groups(region, credentials):
    '''
    Returns a list of all Security Groups in a given region
    :param region: operating region
    :param credentials: credential object
    :return:
    '''
    print('{timestamp}: Getting all Security Groups in {region}..').format(
        timestamp=timestamp(),
        region=region
    ),
    try:
        ec2 = boto3.resource(
            'ec2',
            region_name=region,
            aws_access_key_id=credentials['AccessKeyId'],
            aws_secret_access_key=credentials['SecretAccessKey'],
            aws_session_token=credentials['SessionToken'],
        )
        payload = list(ec2.security_groups.all())
        payload = set([sg.group_id for sg in payload])
        print(' {total} total Security Groups detected').format(
            total=str(len(payload))
        )
    except Exception as e:
        raise(e)
    return payload


def rm_orphaned_security_groups(all_security_groups, region, credentials):
    '''
    Attempts to remove unused Security Groups
    :param all_security_groups: list of Security Groups to be removed
    :param region: operating region
    :param credentials: credentials object
    :return:
    '''
    deleted = []
    skipped = []
    print('{timestamp}: Attempting to remove {total} Security Groups:').format(
        timestamp=timestamp(),
        total=str(len(all_security_groups))
    )

    client = boto3.client(
        'ec2',
        region_name=region,
        aws_access_key_id=credentials['AccessKeyId'],
        aws_secret_access_key=credentials['SecretAccessKey'],
        aws_session_token=credentials['SessionToken'],
    )
    for security_group in all_security_groups:
        print('{timestamp}: {sg}: ').format(
            timestamp=timestamp(),
            sg=security_group
        ),
        try:
            client.delete_security_group(
                GroupId=security_group,
            )
            print('Deleted!')
            deleted.append(security_group)
        except ClientError as e:
            if e.response['Error']['Code'] == 'DependencyViolation':
                print('dependency detected, skipping..')
            elif e.response['Error']['Code'] == 'CannotDelete':
                print('default VPC Security Group, skipping..')
            else:
                print e.response['Error']['Message']
            pass
            skipped.append(security_group)
        except Exception as e:
            raise(e)

    print('{timestamp}: {total} total Security Groups detected, {deleted} deleted and {skipped} skipped.').format(
        timestamp=timestamp(),
        total=str(len(all_security_groups)),
        deleted=str(len(deleted)),
        skipped=str(len(skipped))
    )


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--region", help="enter the desired region")
    parser.add_argument("--role_name", help="enter the role you wish to use")
    args = parser.parse_args()
    return args


def get_credentials(role_name):
    '''
    Returns credentials object after assuming role via STS
    :param role_name: friendly name of role to assume
    :return:
    '''
    print('{timestamp}: Assuming Role: {role_name}..').format(
        timestamp=timestamp(),
        role_name=role_name
    ),
    try:
        client = boto3.client('sts')
        account_id = client.get_caller_identity().get('Account')
        response = client.assume_role(
            RoleArn=('arn:aws:iam::{account_id}:role/{role_name}').format(
                account_id=account_id,
                role_name=role_name
            ),
            RoleSessionName='SgCleanUp',
        )
        credentials = response.get('Credentials')
        print(' Success!')
    except ClientError as e:
        if e.response['Error']['Code'] == 'AccessDenied':
            print('Cannot assume role, verify your trust settings and region')
            print('{timestamp}: Exiting..').format(
                timestamp=timestamp(),
            )
            sys.exit(1)
    except Exception as e:
        raise(e)
    return credentials


if __name__ == '__main__':
    args = get_arguments()
    region = args.region
    role_name = args.role_name
    print('{timestamp}: This script will remove all orphaned Security Groups in {region}').format(
        timestamp=timestamp(),
        region=region
    )
    procede = raw_input('Would you like to proceed? Y/N ')
    if procede == 'N' or procede == 'n' or procede == 'No' or procede == 'no':
        print('{timestamp}: Exiting..').format(
            timestamp=timestamp(),
        )
        sys.exit(0)
    credentials = get_credentials(role_name)
    all_security_groups = get_all_security_groups(region, credentials)
    if len(all_security_groups) > 0:
        rm_orphaned_security_groups(all_security_groups, region, credentials)
        print('{timestamp}: Exiting..').format(
            timestamp=timestamp(),
        )
        sys.exit(0)
    else:
        print('{timestamp}: Exiting..').format(
            timestamp=timestamp(),
        )
        sys.exit(0)