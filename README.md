Description: Tool to remove unused / orphaned security groups from your AWS account

Prerequisites: Python 2.7, BOTO3

Usage:

rm_unused_security_groups.py --region {REGION} --role_name {ROLE_NAME}

Parameters:

--region: the AWS region you wish to affect

--role_name: the friendly name of the role you wish to use. I have included a CloudFormation template to provision a least-priviledge role call RmOrphanedSecurityGroupsRole if you wish to use that (reccomended)


Interactive Version: 

Useage: rm_unused_security_groups_interactive.py