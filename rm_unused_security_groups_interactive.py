#!/usr/bin/env python

from __future__ import unicode_literals
import boto3
from PyInquirer import style_from_dict, Token, prompt
from PyInquirer import Validator, ValidationError
import time
import datetime
import sys
from botocore.exceptions import ClientError


style = style_from_dict({
    Token.QuestionMark: '#E91E63 bold',
    Token.Selected: '#673AB7 bold',
    Token.Instruction: '',  # default
    Token.Answer: '#2196f3 bold',
    Token.Question: '',
})


class RegionValidator(Validator):
    def validate(self, document):
        region_list = [
            'us-west-1',
            'us-west-2',
            'us-east-2',
            'us-east-1',
            'us-gov-east-1',
            'us-gov-west-1',
            'ca-central-1',
            'eu-west-1',
            'eu-west-2',
            'eu-west-3',
            'eu-central-1',
            'eu-north-1',
            'cn-north-1',
            'cn-northwest-1',
            'ap-south-1',
            'ap-east-1',
            'ap-northeast-1',
            'ap-northeast-2',
            'ap-northeast-3',
            'ap-southeast-1',
            'ap-southeast-2',
            'sa-east-1',
        ]
        if document.text not in region_list:
            raise ValidationError(
                message='Please enter a valid AWS Region',
                cursor_position=len(document.text))  # Move cursor to end


def timestamp():
    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')


def get_all_security_groups(region, credentials):
    '''
    Returns a list of all Security Groups in a given region
    :param region: operating region
    :param credentials: credential object
    :return:
    '''
    print('{timestamp}: Getting all Security Groups in {region}..').format(
        timestamp=timestamp(),
        region=region
    ),
    try:
        ec2 = boto3.resource(
            'ec2',
            region_name=region,
            aws_access_key_id=credentials['AccessKeyId'],
            aws_secret_access_key=credentials['SecretAccessKey'],
            aws_session_token=credentials['SessionToken'],
        )
        payload = list(ec2.security_groups.all())
        payload = set([sg.group_id for sg in payload])
        print(' {total} total Security Groups detected').format(
            total=str(len(payload))
        )
    except Exception as e:
        raise(e)
    return payload


def rm_orphaned_security_groups(all_security_groups, region, credentials):
    '''
    Attempts to remove unused Security Groups
    :param all_security_groups: list of Security Groups to be removed
    :param region: operating region
    :param credentials: credentials object
    :return:
    '''
    deleted = []
    skipped = []
    print('{timestamp}: Attempting to remove {total} Security Groups:').format(
        timestamp=timestamp(),
        total=str(len(all_security_groups))
    )

    client = boto3.client(
        'ec2',
        region_name=region,
        aws_access_key_id=credentials['AccessKeyId'],
        aws_secret_access_key=credentials['SecretAccessKey'],
        aws_session_token=credentials['SessionToken'],
    )
    for security_group in all_security_groups:
        print('{timestamp}: {sg}: ').format(
            timestamp=timestamp(),
            sg=security_group
        ),
        try:
            client.delete_security_group(
                GroupId=security_group,
            )
            print('Deleted!')
            deleted.append(security_group)
        except ClientError as e:
            if e.response['Error']['Code'] == 'DependencyViolation':
                print('dependency detected, skipping..')
            elif e.response['Error']['Code'] == 'CannotDelete':
                print('default VPC Security Group, skipping..')
            else:
                print(e.response['Error']['Message'])
            pass
            skipped.append(security_group)
        except Exception as e:
            raise(e)
    print('{timestamp}: {total} total Security Groups detected, {deleted} deleted and {skipped} skipped.').format(
        timestamp=timestamp(),
        total=str(len(all_security_groups)),
        deleted=str(len(deleted)),
        skipped=str(len(skipped))
    )


def get_credentials(role_name):
    '''
    Returns credentials object after assuming role via STS
    :param role_name: friendly name of role to assume
    :return:
    '''
    print('{timestamp}: Assuming Role: {role_name}..').format(
        timestamp=timestamp(),
        role_name=role_name
    ),
    try:
        client = boto3.client('sts')
        account_id = client.get_caller_identity().get('Account')
        response = client.assume_role(
            RoleArn=('arn:aws:iam::{account_id}:role/{role_name}').format(
                account_id=account_id,
                role_name=role_name
            ),
            RoleSessionName='SgCleanUp',
        )
        credentials = response.get('Credentials')
        print(' Success!')
    except ClientError as e:
        if e.response['Error']['Code'] == 'AccessDenied':
            print('Cannot assume role, verify your trust settings and region')
            print('{timestamp}: Exiting..').format(
                timestamp=timestamp(),
            )
            sys.exit(1)
    except Exception as e:
        raise(e)
    return credentials


def get_region():
    question = [
        {
            'type': 'input',
            'name': 'Region',
            'message': 'What Region should we target?',
            'validate': RegionValidator
        }
    ]
    response = prompt(question, style=style)
    return response['Region']


def get_role():
    question = [
        {
            'type': 'input',
            'name': 'Role',
            'message': 'What Role would you like to use?',
            #'validate': RegionValidator
            'default': 'RmOrphanedSecurityGroupsRole'
        }
    ]
    response = prompt(question, style=style)
    return response['Role']


if __name__ == '__main__':
    question = [
        {
            'type': 'confirm',
            'name': 'Continue',
            'message': (
                'Do you wish to continue?'
            ),
            'default': False
        },
    ]
    print('This script will remove all orphaned Security Groups in a given Region')
    response = prompt(question, style=style)
    if response['Continue'] == True:
        region = get_region()
        role_name = get_role()
        credentials = get_credentials(role_name)
        all_security_groups = get_all_security_groups(region, credentials)
        if len(all_security_groups) > 0:
            rm_orphaned_security_groups(all_security_groups, region, credentials)
            print('{timestamp}: Exiting..').format(
                timestamp=timestamp(),
            )
            sys.exit(0)
        else:
            print('{timestamp}: Exiting..').format(
                timestamp=timestamp(),
            )
            sys.exit(0)
    else:
        print('{timestamp}: Exiting..').format(
            timestamp=timestamp(),
        )
        sys.exit(0)